﻿using ApiAngularBiblio.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace ApiAngularBiblio.Interfaces
{
    public interface IAutorRepository : IBaseRepository<Autor>
    {

    }
}
