﻿using System;
using System.Collections.Generic;
using System.Text;
using ApiAngularBiblio.Models;

namespace ApiAngularBiblio.Interfaces
{
    public interface ILibroRepository : IBaseRepository<Libro>
    {

    }
}
