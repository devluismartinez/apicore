﻿using ApiAngularBiblio.Interfaces;
using ApiAngularBiblio.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace ApiAngularBiblio.Interfaces
{
    public interface ICategoriaRepository : IBaseRepository<Categoria>
    {
    }
}
