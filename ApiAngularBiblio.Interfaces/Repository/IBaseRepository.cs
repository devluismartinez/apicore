﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ApiAngularBiblio.Interfaces
{
    public interface IBaseRepository<T> where T : class
    {
        Task Delete(int id);
        Task<List<T>> GetAll();
        Task<T> GetById(int id);
        Task Save();
        Task Store(T libro);
        Task Update(T libro);
    }
}
