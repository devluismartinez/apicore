﻿using ApiAngularBiblio.Interfaces;
using ApiAngularBiblio.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace ApiAngularBiblio.Repositories
{
    public class AutorRepository : BaseRepository<Autor>, IAutorRepository
    {
        public AutorRepository(BiblioContext context) 
        : base(context)
        {
        }
    }
}
