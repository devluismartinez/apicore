﻿using ApiAngularBiblio.Models;
using ApiAngularBiblio.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ApiAngularBiblio.Repositories
{
    public class LibroRepository : BaseRepository<Libro>, ILibroRepository
    {
        public LibroRepository(BiblioContext context)
        : base(context)
        {
        }
    }
}
