﻿using ApiAngularBiblio.Interfaces;
using ApiAngularBiblio.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace ApiAngularBiblio.Repositories
{
    public class CategoriaRepository : BaseRepository<Categoria>, ICategoriaRepository
    {
        public CategoriaRepository(BiblioContext context)
        : base(context)
        {
        }
    }
}
