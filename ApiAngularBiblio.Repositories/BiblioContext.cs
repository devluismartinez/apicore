﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore;
using ApiAngularBiblio.Models;

namespace ApiAngularBiblio.Repositories
{
    public class BiblioContext : DbContext
    {
        public BiblioContext(DbContextOptions<BiblioContext> options) : base(options) { }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<LibroCategoria>().HasKey(lc => new { lc.LibroId, lc.CategoriaId });

            modelBuilder.Entity<LibroCategoria>()
                .HasOne<Libro>(sc => sc.Libro)
                .WithMany(s => s.Categorias)
                .HasForeignKey(sc => sc.LibroId);


            modelBuilder.Entity<LibroCategoria>()
                .HasOne<Categoria>(sc => sc.Categoria)
                .WithMany(s => s.Libros)
                .HasForeignKey(sc => sc.CategoriaId);

        }

        public DbSet<Libro> Libro { get; set; }
        public DbSet<Autor> Autor { get; set; }
        public DbSet<Categoria> Categoria { get; set; }
        public DbSet<LibroCategoria> LibroCategoria { get; set; }

    }
}
