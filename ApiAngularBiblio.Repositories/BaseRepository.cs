﻿using ApiAngularBiblio.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApiAngularBiblio.Repositories
{
    public class BaseRepository<T> : IBaseRepository<T> where T : class
    {
        private readonly BiblioContext _context;

        public BaseRepository(BiblioContext context)
        {
            _context = context;
        }

        public async Task Delete(int id)
        {
           var entity = await GetById(id);
           _context.Set<T>().Remove(entity);
            await Save();
        }

        public async Task<List<T>> GetAll()
        {
            return await _context.Set<T>().ToListAsync();
        }

        public async Task<T> GetById(int id)
        {
            return await _context.Set<T>().FindAsync(id);
        }

        public async Task Save()
        {
            await _context.SaveChangesAsync();
        }

        public async Task Store(T entity)
        {
            await _context.Set<T>().AddAsync(entity);
            await Save();
        }

        public async Task Update(T entity)
        {
            _context.Set<T>().Update(entity);
            await Save();
        }


    }
}
