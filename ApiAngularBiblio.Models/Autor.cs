﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ApiAngularBiblio.Models
{
    public class Autor
    {
        public int? Id { get; set; }
        public string Nombre { get; set; }

    }
}
