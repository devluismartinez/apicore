﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ApiAngularBiblio.Models
{
    public class Categoria
    {
        public int? Id { get; set; }
        public string Nombre { get; set; }

        public IList<LibroCategoria> Libros { get; set; }
    }
}
