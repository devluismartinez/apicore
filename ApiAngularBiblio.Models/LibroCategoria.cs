﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ApiAngularBiblio.Models
{
    public class LibroCategoria
    {
        public int Id { get; set; }
        public int LibroId { get; set; }
        public Libro Libro { get; set; }

        public int CategoriaId{ get; set; }
        public Categoria Categoria { get; set; }
    }
}
