﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Internal;
using System;
using System.Collections.Generic;
using System.Text;

namespace ApiAngularBiblio.Models
{
    public class Libro
    {
        public int? Id { get; set; }
        public string Titulo { get; set; }
        public string Descripcion { get; set; }
        public string Autor { get; set; }
        public string Categoria { get; set; }
        public byte[] Imagen { get; set; }
        public bool? EsPopular { get; set; }

        public IList<LibroCategoria> Categorias { get; set; }
    }
}
