﻿using ApiAngularBiblio.Models;
using ApiAngularBiblio.Interfaces;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ApiAngularBiblio.Controllers
{
    [Produces("application/json")]
    [Route("api/v1/[controller]")]
    [ApiController]
    public class LibrosController : ControllerBase
    {
        private ILibroRepository _libroRepository;
 
        public LibrosController(ILibroRepository libroRepository)
        {
            _libroRepository = libroRepository;
        }

        // GET api/v1/lib/ros
        [HttpGet]
        public async Task<ActionResult<List<Libro>>> GetAll()
        {
            return await _libroRepository.GetAll();
        }

        // GET api/v1/libros/1
        [HttpGet("{id}")]
        public async Task<ActionResult<Libro>> GetById(int id)
        {
            var libro = await _libroRepository.GetById(id);

            if (libro == null)
            {
                return NotFound();
            }

            return libro;
        }

        // Post api/v1/libros
        [HttpPost]
        public async Task<ActionResult<Libro>> Create([FromBody]Libro libro)
        {
            if (libro == null)
            {
                return BadRequest();
            }

            await _libroRepository.Store(libro);

            return CreatedAtAction(nameof(GetById), new { id = libro.Id }, libro);
        }

        [HttpPut("{id}")]
        public async Task<ActionResult<Libro>> Update(int id, [FromBody]Libro libro)
        {
            if (id == 0 || libro == null)
            {
                return BadRequest();
            }

            await _libroRepository.Update(libro);
            return NoContent();
        }

        [HttpDelete("{id}")]
        public async Task<ActionResult<Libro>> Delete(int id)
        {

            if (id == 0)
            {
                return NotFound();
            }

            await _libroRepository.Delete(id);
            return NoContent();
        }


    }
}