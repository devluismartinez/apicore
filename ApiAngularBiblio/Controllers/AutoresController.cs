﻿using ApiAngularBiblio.Interfaces;
using ApiAngularBiblio.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace ApiAngularBiblio.Controllers
{
    [Produces("application/json")]
    [Route("api/v1/[controller]")]
    [ApiController]
    public class AutoresController : ControllerBase
    {
        private IAutorRepository _autorRepository;

        public AutoresController(IAutorRepository autorRepository)
        {
            _autorRepository = autorRepository;
        }

        [HttpGet]
        public async Task<ActionResult<List<Autor>>> GetAll()
        {
            return await _autorRepository.GetAll();
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<Autor>> GetById(int id)
        {
            var autor = await _autorRepository.GetById(id);

            if (autor == null)
            {
                return NotFound();
            }

            return autor;
        }

        [HttpPost]
        public async Task<ActionResult<Autor>> Create([FromBody]Autor autor)
        {
            if (autor == null)
            {
                return BadRequest();
            }

            await _autorRepository.Store(autor);

            return CreatedAtAction(nameof(GetAll), new { id = autor.Id }, autor);

        }

        [HttpPut("{id}")]
        public async Task<ActionResult<Autor>> Update(int id, [FromBody]Autor autor)
        {
            if (id == 0 || autor == null)
            {
                return BadRequest();
            }

            await _autorRepository.Update(autor);
            return NoContent();
        }

        [HttpDelete("{id}")]
        public async Task<ActionResult<Autor>> Delete(int id)
        {

            if (id == 0)
            {
                return NotFound();
            }

            await _autorRepository.Delete(id);
            return NoContent();
        }
    }
}
