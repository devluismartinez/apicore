﻿using ApiAngularBiblio.Interfaces;
using ApiAngularBiblio.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace ApiAngularBiblio.Controllers
{
    [Produces("application/json")]
    [Route("api/v1/[controller]")]
    [ApiController]
    public class CategoriasController : ControllerBase
    {
        private ICategoriaRepository _categoriaRepository;

        public CategoriasController(ICategoriaRepository categoriaRepository)
        {
            _categoriaRepository = categoriaRepository;
        }

        [HttpGet]
        public async Task<ActionResult<List<Categoria>>> GetAll()
        {
            return await _categoriaRepository.GetAll();
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<Categoria>> GetById(int id)
        {
            var autor = await _categoriaRepository.GetById(id);

            if (autor == null)
            {
                return NotFound();
            }

            return autor;
        }

        [HttpPost]
        public async Task<ActionResult<Autor>> Create([FromBody]Categoria categoria)
        {
            if (categoria == null)
            {
                return BadRequest();
            }

            await _categoriaRepository.Store(categoria);

            return CreatedAtAction(nameof(GetAll), new { id = categoria.Id }, categoria);

        }

        [HttpPut("{id}")]
        public async Task<ActionResult<Categoria>> Update(int id, [FromBody]Categoria categoria)
        {
            if (id == 0 || categoria == null)
            {
                return BadRequest();
            }

            await _categoriaRepository.Update(categoria);
            return NoContent();
        }

        [HttpDelete("{id}")]
        public async Task<ActionResult<Categoria>> Delete(int id)
        {

            if (id == 0)
            {
                return NotFound();
            }

            await _categoriaRepository.Delete(id);
            return NoContent();
        }
    }
}
